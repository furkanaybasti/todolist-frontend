const puppeteer = require("puppeteer");
const testTasks = [
    "Kahvaltı yap",
    "Spora git",
    "Ders çalış"
];
const failTasks = [
    "Do",
    "",
    "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolores, unde non. Animi, et eveniet earum nesciunt ab maxime"
];
const errorMessage = "Todo sentence must be greater than 3 characters and less than 100 characters";
(async () => {
    let test1Success = false;
    let test2Success = false;
    console.info('Test start.');
    const browser = await puppeteer.launch(
        {
            "headless": false,
            "slowMo": 10
        });

    const page = await browser.newPage();
    await page.goto('https://dev-62838222.okta.com/');
    await page.type('[id=okta-signin-username]', "furkan_aybasti@hotmail.com");
    await page.type('[id=okta-signin-password]', "todolistapp");
    await page.click('#okta-signin-submit');
    await delay(8000);
    await page.goto('http://localhost:4200/todosPage');
    await delay(8000);

    
    // Eklenen todo veritabanına kaydedildi mi sayfada listeleniyor mu?
    for (let i = 0; i < testTasks.length; i++) {
        await page.type('[id=itemTitleBtn]', testTasks[i]);
        await page.click('#addTitleBtn');
        await delay(5000);
        const text = await page.evaluate(() => Array.from(document.querySelectorAll('[id=liItem]'), element => element.textContent));

        if (text[text.length - 1] != null) {
            if (testTasks[i].trim() == text[text.length - 1].trim()) {
                console.log("The todo sentence have been added to the database and lists on the page");
                console.log("Test 1 success.");
                test1Success = true;
            }
        } else {
            console.log("The todo sentence have not been added to the database and not lists on the page");
            test1Success = false;
            break;
        }
    }

    //Kabul edilmeyen biçimde bir sözcük girince hata mesajı veriyor mu ?
    for (let i = 0; i < failTasks.length; i++) {
        await page.type('[id=itemTitleBtn]', failTasks[i]);
        await page.click('#addTitleBtn');
        await delay(5000);

        if (failTasks[i].length <= 3 || failTasks[i] == null || failTasks[i].trim() == "" || failTasks[i].length >= 100) {
            await page.waitForSelector('[id=errorMsgId]')
            let element = await page.$('[id=errorMsgId]')
            let value = await page.evaluate(el => el.textContent, element);
            console.log("value:" + value);
            console.log("errorMessage:" + errorMessage);
            if (value != null) {
                if (value.trim() == errorMessage) {
                    console.log("Test 2 success.");
                    test2Success = true;
                }
                else {
                    console.log(errorMessage);
                    console.log("Test 2 fail.");
                    test2Success = false;
                    break;
                }
            }
        }
    }
    
    if (test1Success && test2Success)
        console.log("Test successful.");
    else
        console.log("Test failed, check logs.");

    await page.screenshot({
        path: 'screenshot.png'
    });
    function delay(time) {
        return new Promise(function (resolve) {
            setTimeout(resolve, time)
        });
    }
    console.log("Test finish");
    await browser.close();
})().catch(err => { console.log(err) });