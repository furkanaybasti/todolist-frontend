import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { OKTA_CONFIG, OktaAuthModule } from '@okta/okta-angular';
const oktaConfig = {
  issuer: 'https://dev-62838222.okta.com/oauth2/default',
  clientId: '0oa2245rqcifzlbsU5d7',
  redirectUri: window.location.origin + '/todosPage'
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        OktaAuthModule
      ],
      declarations: [
        AppComponent,
      ],
      providers:[{ provide: OKTA_CONFIG, useValue: oktaConfig }]
    }).compileComponents();
  }));

});
