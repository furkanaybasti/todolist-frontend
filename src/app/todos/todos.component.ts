import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  errorMessage;
  events: any[] = [];
  currentEvent: any = { id: null, name: '' };
  modalCallback: () => void;

  constructor(private server: ServerService) { }

  ngOnInit() {
    this.getEvents();
  }

  private updateForm(name) {
    name.value = '';
    this.errorMessage = null;
  }

  private getEvents() {
    this.server.getTodos().then((response: any) => {
      this.events = response.map((ev) => {
        return ev;
      });
      this.events.sort((a, b) => {
        return a.isChecked - b.isChecked;
      });
    });
  }

  addEvent(name) {
    if (name.value.length >= 3 && name.value.length <= 100 && name.value.length != "") {
      this.currentEvent = { id: null, name: '', isChecked: false };
      this.createEvent(name);
      this.updateForm(name);
    } else {
      this.errorMessage = "Todo sentence must be greater than 3 characters and less than 100 characters";
    }
  }

  createEvent(todoName) {
    const newEvent = {
      name: todoName.value,
      isChecked: false
    };
    this.server.createTodos(newEvent).then(() => {
      this.getEvents();
    });
  }

  editEvent(index, name) {
    this.currentEvent = this.events[index];
    this.updateEvent(name);
    this.updateForm(name);
  }

  updateEvent(name) {
    const eventData = {
      id: this.currentEvent.id,
      name: name.value
    };
    console.log("eventData: " + JSON.stringify(eventData));
    this.server.updateTodos(eventData).then(() => {
      this.getEvents();
    });
  }

  deleteEvent(index) {
    this.server.deleteTodos(this.events[index]).then(() => {
      this.getEvents();
    });
  }

  alterCheck(index, isChecked) {
    this.currentEvent = this.events[index];
    const eventData = {
      id: this.currentEvent.id,
      isChecked: !isChecked
    };
    this.server.alterTodos(eventData).then(() => {
      this.getEvents();
    });
  }
}
