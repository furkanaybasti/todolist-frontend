import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TodosComponent } from './todos.component';
import {DebugElement} from '@angular/core';
import { OKTA_CONFIG, OktaAuthModule } from '@okta/okta-angular';
import {By} from '@angular/platform-browser';
const oktaConfig = {
  issuer: 'https://dev-62838222.okta.com/oauth2/default',
  clientId: '0oa2245rqcifzlbsU5d7',
  redirectUri: window.location.origin + '/callback'
}
describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;
  let debugElement: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({

      imports: [RouterTestingModule,HttpClientModule,OktaAuthModule],
      declarations: [TodosComponent],
      providers: [{ provide: OKTA_CONFIG, useValue: oktaConfig }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create todos component', () => {
    expect(component).toBeTruthy();
  });
});
