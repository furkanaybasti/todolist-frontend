<p align="center">
<h1 align="center"> TodoList App</h1>
</p>

This application allows you to create and manage to-do lists using a database.

## Installation

1- Run this command to make the necessary libraries and installations.

```bash
$ npm install
```

2- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
```bash
$ ng serve
```

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running Acceptance tests

1- Run this command to make the necessary libraries and installations.

```bash
$ npm install
```

2- Run `node .\acceptance-testing.js` in acceptance-testing folder to execute acceptance-test via [Puppeteer](https://github.com/puppeteer).

```bash
$ node .\acceptance-testing.js
```

## License

Todolist app is licensed under the [Apache 2.0 license](LICENCE).



